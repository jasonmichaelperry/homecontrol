//
//  HCInformationServices.m
//  HomeControl
//
//  Created by Jason Perry on 11/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HCInformationServices.h"

@implementation HCInformationServices

//
-(id) init
{
    self = [super init];
    if( self ){
        //
        zipCode = @"20009";
    }
    return self;
}

//
-(NSString *) checkWeatherForZip: (NSString *) zip{
    
    //
    NSString *resultString;
    
    //
    NSString *weatherUrl = @"http://api.wunderground.com/api/0d13b124ab9c7c56/conditions/q/DC/Washington.json";
    
    //uses the NSRequest object to determine what to send to the designated URL
    NSMutableURLRequest *hueRequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: weatherUrl]];
    [hueRequest setHTTPMethod:@"GET"];
    [hueRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [hueRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //error message
    NSError *e = nil;
    
    //make a request to learn about the system
    NSData *data = [NSURLConnection sendSynchronousRequest: hueRequest returningResponse:nil error:&e];
    
    //
    NSDictionary *results = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
    
    //encoding succeded
    if( !e ){
      
        //
        NSDictionary *current_observation = [results valueForKey: @"current_observation"];
        
        //
        resultString = [NSString stringWithFormat: @"The weather in Washington DC feels like %@ degrees with signs of %@", (NSString *)[current_observation valueForKey: @"feelslike_f"], (NSString *)[current_observation valueForKey: @"weather"]  ];
        
    }else{
        NSLog( @"failed" );
    }
    
    return resultString;
}

//
-(NSString *) checkStockQoute: (NSString *) stock{
    return @"";
}

//
-(NSString *) checkNewsHeadlines{
    
    //
    NSString *resultString;
    
    //
    NSString *newsUrl = @"https://ajax.googleapis.com/ajax/services/search/news?v=1.0&q=top";
    
    //uses the NSRequest object to determine what to send to the designated URL
    NSMutableURLRequest *hueRequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: newsUrl]];
    [hueRequest setHTTPMethod:@"GET"];
    [hueRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [hueRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //error message
    NSError *e = nil;
    
    //make a request to learn about the system
    NSData *data = [NSURLConnection sendSynchronousRequest: hueRequest returningResponse:nil error:&e];
    
    //
    NSDictionary *results = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
    
    //encoding succeded
    if( !e ){
        
        //
        NSLog( [results description] );
        NSDictionary *responseData = [results valueForKey: @"responseData"];
        NSLog( [responseData description] );
        NSArray *resultsData = [responseData valueForKey: @"results"];
        NSLog( [resultsData description] );
        
        for ( NSDictionary *newsItem in resultsData) {
            [resultString stringByAppendingFormat: [newsItem valueForKey: @"content"]];
        }
        
    }else{
        NSLog( @"failed" );
    }
    
    return resultString;    
}


@end
