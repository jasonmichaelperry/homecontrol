//
//  HCSpeechRecognizer.h
//  HomeControl
//
//  Created by Jason Perry on 11/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HCInformationServices.h"
#import "HCHueService.h"


//
@interface HCSpeechRecognizer : NSObject<NSSpeechRecognizerDelegate>{
    NSSpeechRecognizer *speechRecognizer;
    NSSpeechSynthesizer *speechSynth;
    HCInformationServices *infoService;
    HCHueService *hueService;
    NSString *currentColor;
    BOOL voiceControlActive;
    BOOL muteResponse;
}

@end
