//
//  HCSpeechRecognizer.m
//  HomeControl
//
//  Created by Jason Perry on 11/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HCSpeechRecognizer.h"

@implementation HCSpeechRecognizer

-(id) init
{
    self = [super init];
    if( self ){
    
        //
        NSArray *commands = [NSArray arrayWithObjects: 
                             @"activate voice control 3",
                             @"activate voice 3",
                             @"code 3",
                             @"activate code 3",
                             @"lock voice control",
                             @"voice control off",
                             @"kill voice control off",
                             @"kill voice control",
                             @"mute",                  
                             @"no response", 
                             @"mute voice",                              
                             @"mute off",                  
                             @"response on", 
                             @"voice on",
                             @"turn kitchen on", 
                             @"turn kitchen off", 
                             @"kitchen on", 
                             @"kitchen off", 
                             @"kitchen lights on", 
                             @"kitchen lights off", 
                             @"turn den on", 
                             @"turn den off", 
                             @"turn den lights on", 
                             @"turn den lights off", 
                             @"den on", 
                             @"den off", 
                             @"turn lights on", 
                             @"turn lights off", 
                             @"lights on", 
                             @"lights off", 
                             @"lights", 
                             @"dim lights",
                             @"dim",
                             @"emergancy",
                             @"set to white",
                             @"set to red",
                             @"set to purple",
                             @"set to green",
                             @"set to yellow",
                             @"set to orange",                             
                             @"set to blue",
                             @"set to dark",
                             @"set to mood",
                             @"set to morning",
                             @"set to evening",
                             @"set to night",
                             @"set to concentrate",
                             @"set to relax",
                             @"set to reading",
                             @"set to movie",
                             @"set to games",
                             @"sexy time",
                             @"change to white",
                             @"change to red",
                             @"change to purple",                             
                             @"change to green",
                             @"change to yellow",
                             @"change to orange",
                             @"change to blue",
                             @"change to dark",
                             @"change to mood",
                             @"change to morning",
                             @"change to evening",
                             @"change to night",
                             @"change to concentrate",
                             @"change to relax",
                             @"change to reading",
                             @"change to movie",
                             @"change to games",
                             @"whats the weather",
                             @"what is the weather",
                             @"the weather",
                             @"weather",
                             @"news",
                             @"todays news",
                             @"top news",                             
                             nil];
        
        //
        muteResponse = NO;
        voiceControlActive = YES;
        
        //
        currentColor = kHCWhite;
        
        //creates a speech recognizer to listen for vocal statements
        speechRecognizer = [[NSSpeechRecognizer alloc] init];
        [speechRecognizer setCommands: commands];
        [speechRecognizer setDelegate: self];
        [speechRecognizer setListensInForegroundOnly: NO];
        [speechRecognizer setBlocksOtherRecognizers: YES];
        [speechRecognizer startListening];
        
        //
        speechSynth = [[NSSpeechSynthesizer alloc] initWithVoice: nil];
        
        //
        infoService = [[HCInformationServices alloc] init];
        
        //
        hueService = [[HCHueService alloc] init];

    }
    return self;
}

-(void) speechRecognizer:(NSSpeechRecognizer *)sender didRecognizeCommand:(id)command{

    //
    NSString *spokenCommand = (NSString *) command;
    NSString *spokenResponse;
    
    //
    //NSLog( spokenCommand );
    
    //
    if( [spokenCommand isEqualToString: @"activate voice control 3"] || [spokenCommand isEqualToString: @"activate voice 3"] || [spokenCommand isEqualToString: @"activate code 3"] || [spokenCommand isEqualToString: @"code 3"] )
    {
        voiceControlActive = YES;
    }
    else if( [spokenCommand isEqualToString: @"lock voice control"] || [spokenCommand isEqualToString: @"voice control off"] || [spokenCommand isEqualToString: @"kill voice control off"] || [spokenCommand isEqualToString: @"kill voice control"] ){
        voiceControlActive = NO;
    }
    
    //
    if( voiceControlActive == NO ){
        [speechSynth startSpeakingString: @"voice control is not active"];
        return;
    }
    
    //
    if( [spokenCommand isEqualToString: @"turn lights on"] || [spokenCommand isEqualToString: @"lights on"] || [spokenCommand isEqualToString: @"lights"] ){
        
        //trigger an alert to turn on all lights
        [hueService turnAllBulbsOnWithJSON: currentColor];
        
        //vocal alert of the operation
        spokenResponse = @"lights are on";
    }

    else if ( [spokenCommand isEqualToString: @"mute"] || [spokenCommand isEqualToString: @"no response"] || [spokenCommand isEqualToString: @"mute voice"]) {
        
        //
        muteResponse = YES;
        
        //vocal alert of the operation
        spokenResponse = @"i'm muting my responses";
    }
    else if ( [spokenCommand isEqualToString: @"mute off"] || [spokenCommand isEqualToString: @"response on"] ||  [spokenCommand isEqualToString: @"voice on"]) {
        
        //turns all lights off
        muteResponse = NO;
        
        //vocal alert of the operation
        spokenResponse = @"mute is off";
    }
    
    else if ( [spokenCommand isEqualToString: @"turn lights off"] || [spokenCommand isEqualToString: @"lights off"]) {
        
        //turns all lights off
        [hueService turnAllBulbsOff];
        
        //vocal alert of the operation
       spokenResponse = @"lights are off";
    }
    else if ([spokenCommand isEqualToString: @"turn kitchen on"] || [spokenCommand isEqualToString: @"kitchen lights on"] || [spokenCommand isEqualToString: @"kitchen on"]) {
        
        //
        [hueService turnOnBulbWithId: 4 withJSON: currentColor];

        //vocal alert of the operation
        spokenResponse = @"kitchen light on";    
    }
    else if ([spokenCommand isEqualToString: @"turn kitchen off"] || [spokenCommand isEqualToString: @"kitchen lights off"] || [spokenCommand isEqualToString: @"kitchen off"]) {
        
        //
        [hueService turnOffBulbWithId: 4];

        //vocal alert of the operation
       spokenResponse = @"kitchen light off";    
    }
    else if ([spokenCommand isEqualToString: @"turn den on"] || [spokenCommand isEqualToString: @"turn den lights on"] || [spokenCommand isEqualToString: @"den on"]) {
        
        //
        [hueService turnOnBulbWithId: 1 withJSON: currentColor];
        [hueService turnOnBulbWithId: 2 withJSON: currentColor];
        [hueService turnOnBulbWithId: 3 withJSON: currentColor];
        
        //vocal alert of the operation
        spokenResponse = @"den lights on";        
    }
    else if ([spokenCommand isEqualToString: @"turn den off"] || [spokenCommand isEqualToString: @"turn den lights off"] || [spokenCommand isEqualToString: @"den off"]) {
        
        //
        [hueService turnOffBulbWithId: 1];
        [hueService turnOffBulbWithId: 2];
        [hueService turnOffBulbWithId: 3];
        
        //vocal alert of the operation
        spokenResponse = @"den lights off";        
    }    
    else if ([spokenCommand isEqualToString: @"dim lights"] || [spokenCommand isEqualToString: @"dim"] ) {
        
        //
        [hueService turnOnBulbWithId: 1 withJSON: kHCDim];
        [hueService turnOnBulbWithId: 2 withJSON: kHCDim];
        [hueService turnOnBulbWithId: 3 withJSON: kHCDim];
        [hueService turnOnBulbWithId: 4 withJSON: kHCDim];
        
        //vocal alert of the operation
        spokenResponse = @"dimming lights";        
    }      
    else if ( [spokenCommand isEqualToString: @"emergancy"] ) {
        
        //turns all lights off
        [hueService triggerAlertOnAllBulbs];
        
        //vocal alert of the operation
        spokenResponse = @"emergancy lights are on, alerting 911.";
    }
    else if ( [spokenCommand isEqualToString: @"sexy time"] ) {
        
        //turns all lights off
        [hueService turnAllBulbsOnWithJSON: kHCDim];
        
        //vocal alert of the operation
       spokenResponse = @"good luck jason, shes much better than your right hand.";  
    }
    else if ( [spokenCommand isEqualToString: @"whats the weather"] || [spokenCommand isEqualToString: @"what is the weather"] || [spokenCommand isEqualToString: @"the weather"] || [spokenCommand isEqualToString: @"weather"] ) {
        
        //vocal alert of the operation
        spokenResponse = [infoService checkWeatherForZip: @"20009"];
    }
    else if ( [spokenCommand isEqualToString: @"news"] || [spokenCommand isEqualToString: @"top news"] || [spokenCommand isEqualToString: @"todays news"] ) {
        
        //vocal alert of the operation
        spokenResponse = [infoService checkNewsHeadlines];
    }
    else if( [spokenCommand rangeOfString: @"set to " ].location != NSNotFound || [spokenCommand rangeOfString: @"change to " ].location != NSNotFound ){
        NSString *valueSaid;
        
        //what color
        if( [spokenCommand rangeOfString: @"white" ].location != NSNotFound ){
            valueSaid = @"white";
            currentColor = kHCWhite;
        }
        else if( [spokenCommand rangeOfString: @"red" ].location != NSNotFound ){
            valueSaid = @"red";
            currentColor = kHCRed;
        }
        else if( [spokenCommand rangeOfString: @"green" ].location != NSNotFound ){
            valueSaid = @"green";
            currentColor = kHCGreen;
        }
        else if( [spokenCommand rangeOfString: @"yellow" ].location != NSNotFound ){
            valueSaid = @"yellow";
            currentColor = kHCYellow;
        }
        else if( [spokenCommand rangeOfString: @"orange" ].location != NSNotFound ){
            valueSaid = @"orange";
            currentColor = kHCOrange;
        }
        else if( [spokenCommand rangeOfString: @"blue" ].location != NSNotFound ){
            valueSaid = @"blue";
            currentColor = kHCBlue;
        }
        else if( [spokenCommand rangeOfString: @"purple" ].location != NSNotFound ){
            valueSaid = @"purple";
            currentColor = kHCBlue;
        }
        else if( [spokenCommand rangeOfString: @"dark" ].location != NSNotFound ){
            valueSaid = @"dark";
            currentColor = kHCDark;
        }
        else if( [spokenCommand rangeOfString: @"mood" ].location != NSNotFound ){
            valueSaid = @"mood";
            currentColor = kHCMood;
        }
        else if( [spokenCommand rangeOfString: @"morning" ].location != NSNotFound ){
            valueSaid = @"morning";
            currentColor = kHCMorning;
        }
        else if( [spokenCommand rangeOfString: @"evening" ].location != NSNotFound ){
            valueSaid = @"evning";
            currentColor = kHCEvening;
        }
        else if( [spokenCommand rangeOfString: @"night" ].location != NSNotFound ){
            valueSaid = @"night";
            currentColor = kHCNight;
        }
        else if( [spokenCommand rangeOfString: @"concentrate" ].location != NSNotFound ){
            valueSaid = @"concentrate";
            currentColor = kHCConcentrate;
        }
        else if( [spokenCommand rangeOfString: @"relax" ].location != NSNotFound ){
            valueSaid = @"relax";
            currentColor = kHCRelax;
        }
        else if( [spokenCommand rangeOfString: @"reading" ].location != NSNotFound ){
            valueSaid = @"reading";
            currentColor = kHCRelax;
        }
        else if( [spokenCommand rangeOfString: @"movie" ].location != NSNotFound ){
            valueSaid = @"movie";
            currentColor = kHCMovie;
        }
        else if( [spokenCommand rangeOfString: @"games" ].location != NSNotFound ){
            valueSaid = @"games";
            currentColor = kHCGames;
        }        
        else if( [spokenCommand rangeOfString: @"sexy" ].location != NSNotFound ){
            valueSaid = @"sexy";
            currentColor = kHCSexy;
        }
        
        //trigger an alert to turn on all lights
        [hueService turnAllBulbsOnWithJSON: currentColor];
        
        //vocal alert of the operation
        spokenResponse = [NSString stringWithFormat: @"setting %@ applied", valueSaid];         
    }
    
    //
    if( muteResponse == NO ){
        [speechSynth startSpeakingString: spokenResponse];
    
    }
}

-(void) dealloc{
    [speechRecognizer stopListening];
}


@end
