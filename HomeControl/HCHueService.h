//
//  HCHueService.h
//  HomeControl
//
//  Created by Jason Perry on 11/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

//
static NSString * const kHCRed = @"{\"bri\":200,\"xy\":[0.412,0.213],\"on\":true}";
static NSString * const kHCGreen = @"{\"bri\":200,\"xy\":[0.358,0.715],\"on\":true}";
static NSString * const kHCYellow = @"";
static NSString * const kHCOrange = @"";
static NSString * const kHCBlue = @"{\"bri\":200,\"xy\":[0.180,0.072],\"on\":true}";
static NSString * const kHCWhite = @"{\"bri\":200,\"xy\":[0.950,1.0],\"on\":true}";
static NSString * const kHCDim = @"{\"bri\":10,\"xy\":[0.950,1.0],\"on\":true}";
static NSString * const kHCDark = @"";
static NSString * const kHCMood = @"";
static NSString * const kHCMorning = @"";
static NSString * const kHCEvening = @"";
static NSString * const kHCNight = @"";
static NSString * const kHCConcentrate = @"";
static NSString * const kHCRelax = @"";
static NSString * const kHCReading = @"";
static NSString * const kHCMovie = @"";
static NSString * const kHCGames = @"";
static NSString * const kHCSexy = @"";

//
@interface HCHueService : NSObject<NSURLConnectionDelegate>
{
    NSDictionary *hueInfo;
    NSString *defaultColor;
    NSString *basePath;
    NSString *hash;
    NSArray *bulbs;
    NSArray *groups;
}

//information calls
-(void) getHueConfiguration;
//-(void) getBulbNames;
//-(void) getBulbGroups;

//update calls
//-(void) changeBulbNameTo: (NSString *) bulbName withId: (int) bulbId; //{"name":"???"}

//entrie house actions
-(void) turnAllBulbsOn;
-(void) turnAllBulbsOnWithJSON: (NSString *) json;
-(void) turnAllBulbsOff;
-(void) triggerAlertOnAllBulbs;

//single light actions
-(void) turnOnBulbWithId: (int) bulbId colorX: (float) colorX andColorY: (float) colorY andBrightness: (int) bri;
-(void) turnOnBulbWithId: (int) bulbId withJSON: (NSString *) json;
-(void) turnOffBulbWithId: (int) bulbId;
-(void) triggerAlertOnBulbWithId: (int) bulbId;  //{"alert":"lselect"}

//group actions
//-(void) turnOnInGroupWithId: (int) groupId;
//-(void) turnOffInGroupWithId: (int) groupId; //{"on":false}

@end
