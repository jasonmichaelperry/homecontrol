//
//  HCInformationServices.h
//  HomeControl
//
//  Created by Jason Perry on 11/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HCInformationServices : NSObject{
    NSString *zipCode;
}

-(NSString *) checkWeatherForZip: (NSString *) zip;
-(NSString *) checkStockQoute: (NSString *) stock;
-(NSString *) checkNewsHeadlines;

@end
