//
//  HCHueService.m
//  HomeControl
//
//  Created by Jason Perry on 11/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HCHueService.h"

@implementation HCHueService

//
-(id) init
{
    self = [super init];
    if( self ){
        
        //set the basepath and application key
        basePath = @"http://10.0.1.2";
        hash = @"0123456789abdcef0123456789abcdef";
        defaultColor = kHCWhite;
        
        //
        [self getHueConfiguration];
        
    }
    return self;
}

//
-(void) getHueConfiguration{
    //creates the path to our hue system
    NSString *apiUrl = [NSString stringWithFormat: @"%@/api/%@", basePath, hash];      
    
    //uses the NSRequest object to determine what to send to the designated URL
    NSMutableURLRequest *hueRequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: apiUrl]];
    [hueRequest setHTTPMethod:@"GET"];
    [hueRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [hueRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //error message
    NSError *e = nil;
    
    //make a request to learn about the system
    NSData *data = [NSURLConnection sendSynchronousRequest: hueRequest returningResponse:nil error:&e];
    
    NSDictionary *results = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
    
    //encoding succeded
    if( !e ){
        hueInfo = results;
        bulbs = [hueInfo valueForKey: @"lights"];
        groups = [hueInfo valueForKey: @"groups"];
    } 
}

//
-(void) turnAllBulbsOn{

    //for now we assume we have 4 lights
    for (NSString* key in bulbs) {
        //turn on each light
        [self turnOnBulbWithId: [key intValue] withJSON: defaultColor];
    }
}

//
-(void) turnAllBulbsOnWithJSON: (NSString *) json{
    for (NSString* key in bulbs) {
        //turn on each light
        [self turnOnBulbWithId: [key intValue] withJSON: json];
    }
}

//
-(void) turnAllBulbsOff{
    //for now we assume we have 4 lights
    for (NSString* key in bulbs) {
        //turn of each light
        [self turnOffBulbWithId: [key intValue]];
    }
}

//
-(void) triggerAlertOnAllBulbs{
    //for now we assume we have 4 lights
     for (NSString* key in bulbs) {
        //turn of each light
        [self triggerAlertOnBulbWithId: [key intValue]];
    }
}

//
//single light actions

//
-(void) turnOnBulbWithId: (int) bulbId colorX: (float) colorX andColorY: (float) colorY andBrightness: (int) bri{
    
    //creates the path to our light
    NSString *apiUrl = [NSString stringWithFormat: @"%@/api/%@/lights/%i/state", basePath, hash, bulbId];
    
    //
    NSString *json = [NSString stringWithFormat: @"{\"bri\":%i,\"xy\":[%f,%f],\"on\":true}", bri, colorX, colorY];
    
    //uses the NSRequest object to determine what to send to the designated URL
    NSMutableURLRequest *bulbRequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: apiUrl]];
    [bulbRequest setHTTPMethod:@"PUT"];
    [bulbRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [bulbRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [bulbRequest setHTTPBody: [NSData dataWithData: [json dataUsingEncoding: NSStringEncodingConversionAllowLossy] ] ];
    
    //
    [NSURLConnection connectionWithRequest: bulbRequest delegate: self];   
}

-(void) turnOnBulbWithId: (int) bulbId withJSON: (NSString *) json{
    //creates the path to our light
    NSString *apiUrl = [NSString stringWithFormat: @"%@/api/%@/lights/%i/state", basePath, hash, bulbId];
    
    //uses the NSRequest object to determine what to send to the designated URL
    NSMutableURLRequest *bulbRequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: apiUrl]];
    [bulbRequest setHTTPMethod:@"PUT"];
    [bulbRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [bulbRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [bulbRequest setHTTPBody: [NSData dataWithData: [json dataUsingEncoding: NSStringEncodingConversionAllowLossy] ] ];
    
    //
    [NSURLConnection connectionWithRequest: bulbRequest delegate: self];   
}

//
-(void) turnOffBulbWithId: (int) bulbId{
    
    //creates the path to our light
    NSString *apiUrl = [NSString stringWithFormat: @"%@/api/%@/lights/%i/state", basePath, hash, bulbId];
    
    //uses the NSRequest object to determine what to send to the designated URL
    NSMutableURLRequest *bulbRequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: apiUrl]];
    [bulbRequest setHTTPMethod:@"PUT"];
    [bulbRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [bulbRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [bulbRequest setHTTPBody: [NSData dataWithData: [@"{\"on\": false}" dataUsingEncoding: NSStringEncodingConversionAllowLossy] ] ];
    
    //
    [NSURLConnection connectionWithRequest: bulbRequest delegate: self];   
}

//
-(void) triggerAlertOnBulbWithId: (int) bulbId{
    
    //creates the path to our light
    NSString *apiUrl = [NSString stringWithFormat: @"%@/api/%@/lights/%i/state", basePath, hash, bulbId];
    
    //uses the NSRequest object to determine what to send to the designated URL
    NSMutableURLRequest *bulbRequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: apiUrl]];
    [bulbRequest setHTTPMethod:@"PUT"];
    [bulbRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [bulbRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [bulbRequest setHTTPBody: [NSData dataWithData: [@"{\"bri\":230,\"xy\":[0.63531,0.34127],\"alert\":\"lselect\"}" dataUsingEncoding: NSStringEncodingConversionAllowLossy] ] ];
    
    //
    [NSURLConnection connectionWithRequest: bulbRequest delegate: self];   
}

//Alerts me that my call to the backend succeded and returns data
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    //receive a string representation of the data returned
    //NSString *jsonResponse = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    
    //
    NSError *e = nil;
    //NSDictionary *results = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
    
    //encoding succeded
    if( !e ){
        
        //what do do with the response?
        
        //NSLog( [results description] );

    //attempt to encode failed
    }else{
        //NSLog( [e description] );
        //NSLog( [e debugDescription] );
    }
                  
    //
    //NSlog( [results description] );
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    //NSLog( [error description] );
}

@end
